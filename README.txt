Shows how to write a cron task that notifies site users if a credit card they used
for a recurring order is about to expire.

Requires Ubercart core & the UC Recurring module.

If those are enabled, should just be able to install as normal.

All code licensed under the GPL, as per Drupal.org guidelines.

TODO:
-----

* Make the cron task into a Conditional Action.
** Might require writing a generic time-based Conditional Action trigger
   (separate module?).
* Make the expiration notification email text customizable.
* Add a settings page to define how far in advance to notify users.
* Make the notification more granular than by expiration month, if possible.
* Add a database table for setting if users have been notified, instead of just
  notifying within a particular window.